# This file gets used during installation to generate
# the code names for sections.  Other names can be used;
# these are just the predefined ones to help avoid typo errors

#likelihoods
likelihoods

#input parameters
cosmological_parameters
halo_model_parameters
intrinsic_alignment_parameters
baryon_parameters
shear_calibration_parameters
number_density_params

# source number density
wl_number_density

# 2-pt and related quantities
matter_power_nl
matter_power_lin
shear_xi
shear_cl
galaxy_cl
cmb_cl
lss_autocorrelation
gal_matter_power_lin
linear_cdm_transfer
sigma_r_lin
sigma_r_nl

#nuisance parameters
planck
intrinsic_alignment_field
shear_calibration

# galaxy bias
bias_field

#background quantities
distances
de_equation_of_state
